All the instructions are available here. (https://docs.docker.com/engine/install/ubuntu/)

## REQUIREMENTS

Make sure you have one of these OS versions installed:

	º Ubuntu Eoan 19.10
	º Ubuntu Bionic 18.04 (LTS)
	º Ubuntu Xenial 16.04 (LTS)

## UNINSTALL PREVIOUS VERSIONS

Identify installed packages:
	$ dpkg -l | grep -i docker
Purge any previous installation:
	$ sudo apt-get purge -y docker-engine docker docker.io docker-ce docker-ce-cli 
	$ sudo apt-get autoremove -y --purge docker-engine docker docker.io docker-ce

If you want to remove containers, images or any trace of those previous installations, use this:
	$ sudo rm -rf /var/lib/docker /etc/docker 
	$ sudo rm /etc/apparmor.d/docker 
	$ sudo groupdel docker 
	$ sudo rm -rf /var/run/docker.sock

# INSTALLATION 

1 - Update the apt package and install docker.io

	$ sudo apt-get update

	$ sudo apt-get install -y docker.io 

2 - Install docker-compose

	$ sudo apt-get install -y curl
	
	$ sudo curl -L "https://github.com/docker/compose/releases/download/1.28.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

	$ sudo chmod +x /usr/local/bin/docker-compose

3 - Add your user to the docker group so you don’t need SUDO

	$ sudo gpasswd -a {user-name} docker

4 - Try to run hello-world image

	$ sudo docker run hello-world

	If everything went right and you don’t have any image on you machine, it will show you a message indicating it wasn’t possible to find hello-world locally. Then the download of the image will start, and right after, there should be a message saying everything is working.



# RUNNING THE ENVIRONMENT USING DOCKER-COMPOSE


1 - Locate the .yml file using your command terminal. While in its folder you can use the following commands to start a match:
	
	$ docker-compose up -d rcssserver3d

	$ docker-compose up -d {first_team_name}

	$ docker-compose up -d {second_team_name}
	
	(In case you didn’t add you user to the docker group, make sure to use sudo)

2 - Now if you want to end the execution of the services, just type:

	$ docker-compose down {service_name}

	Or, if you want to take down all of the running containers, just type:
	
	$ docker-compose down

	(once again, make sure to use sudo in case you didn’t add you user to the docker group)

obs: The -d flag is supose to run the container in a detached mode, so it gives the terminal control back to you.

